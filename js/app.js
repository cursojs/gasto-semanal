// const presupuestoSemanal = prompt('Cual es tu presupuesto semanal');
const montoTotal = document.querySelector('#total');
const montoRestante = document.querySelector('#restante');
const formulario = document.querySelector('#agregar-gasto');
let cantidadPresupuesto;

getPresupuestoSemanal();
async function getPresupuestoSemanal() {
    const {value: presupuesto} = await Swal.fire({
        title: 'Ingresa tu presupuesto',
        input: 'number',
        allowOutsideClick: false,
        inputPlaceholder: '5000',
        inputValidator: (value) => 
            !value ? 'El presupuesto es requerido' : null
    });
    cantidadPresupuesto = new Presupuesto(presupuesto);

    const ui = new Interfaz();
    ui.insertarPresupuesto(cantidadPresupuesto.presupuesto);
};

class Presupuesto {
    constructor(presupuesto) {
        this.presupuesto = Number(presupuesto);
        this.restante = Number(presupuesto);
    }

    presupuestoRestante(cantidad = 0) {
        return this.restante -= cantidad;
    }
}

class Interfaz {
    insertarPresupuesto(cantidad) {
        console.log(cantidad);
        montoTotal.textContent = cantidad;
        montoRestante.textContent = cantidad;
    }

    imprimirMensaje(mensaje, tipo) {
        const div = document.createElement('div');
        div.classList.add('text-center', 'alert');
        div.classList.add(tipo === 'error' ? 'alert-danger' : 'alert-success');

        div.appendChild(document.createTextNode(mensaje));
        document.querySelector('.primario').insertBefore(div, formulario);
        setTimeout(() => {
            document.querySelector('.primario .alert').remove();
            formulario.reset();
        }, 2000);
    }

    agregarGastoListado(nombre, cantidad) {
        const gastosListado = document.querySelector('#gastos ul');
        const li = document.createElement('li');
        li.className = 'list-group-item d-flex justify-content-between align-items-center';
        li.innerHTML = `
            ${nombre}
            <span class="badge badge-primary badge-pill"> $ ${cantidad}</span>
        `;
        
        gastosListado.appendChild(li);
    }

    presupuestoRestante(cantidad) {
        cantidadPresupuesto.presupuestoRestante(cantidad);
        montoRestante.textContent -= cantidad;
        this.comprobarPresupuesto();
    }

    comprobarPresupuesto() {
        console.log(cantidadPresupuesto);
        const { presupuesto, restante } = cantidadPresupuesto;
        const divrestante = document.querySelector('.restante');

        if (presupuesto / 4 > restante) {
            divrestante.classList.remove('alert-success', 'alert-warning');
            divrestante.classList.add('alert-danger');
        } else if (presupuesto / 2 > restante) {
            divrestante.classList.remove('alert-success', 'alert-danger');
            divrestante.classList.add('alert-warning');
        }
        
    }
}

formulario.addEventListener('submit', function (e) {
    e.preventDefault();
    const nombre = document.querySelector('#gasto').value;
    const cantidad = Number(document.querySelector('#cantidad').value);
    const restante = Number(montoRestante.textContent);
    console.log(`cantidad ${cantidad} ${typeof(cantidad)} y restante ${restante} ${typeof(restante)}`);
    
    const ui = new Interfaz();
    if(nombre === '' || cantidad === '') {
        ui.imprimirMensaje('Hubo un error', 'error');
    } else if(cantidad > restante) {
        ui.imprimirMensaje(`Su gasto de ${cantidad} es mayor al saldo restante`, 'error');
    } else {
        ui.imprimirMensaje('Gasto agregado', 'correcto');
        ui.agregarGastoListado(nombre, cantidad);
        ui.presupuestoRestante(cantidad);
    }
});
